set nocompatible

let mapleader = "\<Space>"

call plug#begin()
Plug 'chriskempson/base16-vim'
Plug 'arcticicestudio/nord-vim'

Plug 'easymotion/vim-easymotion'

Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': 'install --all' }
Plug 'junegunn/fzf.vim'
Plug 'junegunn/goyo.vim', { 'for': 'markdown' } | Plug 'junegunn/limelight.vim'

Plug 'vim-airline/vim-airline' | Plug 'vim-airline/vim-airline-themes'
Plug 'edkolev/tmuxline.vim'
Plug 'tmux-plugins/vim-tmux-focus-events'
Plug 'roxma/vim-tmux-clipboard'

Plug 'mattn/calendar-vim'
Plug 'vimwiki/vimwiki'
Plug 'tbabej/taskwiki', { 'do': 'pip3 install --upgrade -r requirements.txt' }
Plug 'xarthurx/taskwarrior.vim'

Plug 'janko/vim-test'

" User text objects

Plug 'kana/vim-textobj-user'
Plug 'whatyouhide/vim-textobj-erb'
Plug 'adriaanzon/vim-textobj-matchit'
Plug 'sgur/vim-textobj-parameter'
Plug 'andyl/vim-textobj-elixir'
Plug 'jasonlong/vim-textobj-css'
Plug 'tek/vim-textobj-ruby'
Plug 'glts/vim-textobj-comment'

Plug 'mhinz/vim-signify'

Plug 'posva/vim-vue'
Plug 'sheerun/vim-polyglot'

Plug 'w0rp/ale'
Plug 'SirVer/ultisnips'
Plug 'prabirshrestha/async.vim'
Plug 'prabirshrestha/vim-lsp'
Plug 'thomasfaingnaert/vim-lsp-snippets'
Plug 'thomasfaingnaert/vim-lsp-ultisnips'
Plug 'prabirshrestha/asyncomplete.vim'
Plug 'prabirshrestha/asyncomplete-lsp.vim'
Plug 'prabirshrestha/asyncomplete-emoji.vim'
Plug 'prabirshrestha/asyncomplete-file.vim'
Plug 'prabirshrestha/asyncomplete-ultisnips.vim'
Plug 'liuchengxu/vista.vim'

Plug 'tpope/vim-dispatch'
Plug 'tpope/vim-commentary'
Plug 'suy/vim-context-commentstring'
Plug 'tpope/vim-fugitive'
Plug 'shumphrey/fugitive-gitlab.vim'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-unimpaired'
Plug 'tpope/vim-obsession'
Plug 'tpope/vim-projectionist'
Plug 'tpope/vim-vinegar'

if !has('win32') && !has('win64')
  Plug 'tpope/vim-eunuch'
endif

Plug 'RRethy/vim-hexokinase'

Plug 'dhruvasagar/vim-prosession'

Plug 'idanarye/vim-merginal'
Plug 'christoomey/vim-tmux-navigator'

Plug 'tmhedberg/matchit'

Plug 'https://gitlab.com/pifpafpouf/gitlab.nvim'

Plug 'c-brenn/fuzzy-projectionist.vim'
call plug#end()

" Keymappings
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

tnoremap <C-J> <C-W>j
tnoremap <C-K> <C-W>k
tnoremap <C-L> <C-W>l
tnoremap <C-H> <C-W>h

nnoremap <Leader>` :botright new <bar> :exe "resize " . (winheight(0) * 2/3) <bar> setlocal wfh <bar> :terminal<CR>
nnoremap <Leader>x :w <bar> bd<CR>

if has('gui_running')
  let g:terminal_ansi_colors = [
    \ '#272822',
    \ '#f92672',
    \ '#a6e22e',
    \ '#f4bf75',
    \ '#66d9ef',
    \ '#ae81ff',
    \ '#a1efe4',
    \ '#f8f8f2',
    \ '#75715e',
    \ '#f92672',
    \ '#a6e22e',
    \ '#f4bf75',
    \ '#66d9ef',
    \ '#ae81ff',
    \ '#a1efe4',
    \ '#f9f8f5'
    \ ]
endif

" Various vim settings
set number
set noshowmode
set ignorecase
set smartcase
set hidden
set signcolumn=yes
set encoding=utf-8
set fileencoding=utf-8
set background=dark
set laststatus=2
set backspace=2
set splitbelow
set splitright
set diffopt+=vertical
set noerrorbells visualbell t_vb=
set nowrap
set sessionoptions-=terminal
set autoread
set wildmenu
if !isdirectory($HOME . "/.vim/swapfiles")
  call mkdir($HOME . "/.vim/swapfiles", "p", 0700)
endif
set directory=$HOME/.vim/swapfiles//
if !isdirectory($HOME . "/.vim/backup")
  call mkdir($HOME . "/.vim/backup", "p", 0700)
endif
set backupdir=$HOME/.vim/backup//
au GUIEnter * set visualbell t_vb=

set tgc
if !has('nvim')
  let base16colorspace=256
endif

if has('nvim')
  set nohlsearch
endif


if !&scrolloff
  set scrolloff=1
endif

if !&sidescrolloff
  set sidescrolloff=5
endif


if &listchars ==# 'eol:$'
  set listchars=tab:>\ ,trail:-,extends:>,precedes:<,nbsp:+
endif


if executable('rg')
  set grepprg=rg\ --vimgrep
endif

" Plain GUI
set guioptions-=m
set guioptions-=M
set guioptions-=t
set guioptions-=T
set guioptions-=r
set guioptions-=R
set guioptions-=l
set guioptions-=L

" Enable syntax and filetypes and set patterns for filetypes
syntax on
filetype plugin indent on
au BufRead,BufNewFile *.l setlocal filetype=lisp
au BufRead,BufNewFile *.md setlocal filetype=markdown
au BufRead,BufNewFile *.cfg,*.spc setlocal filetype=xml

" Keep things at a specific line length
highlight OverLength ctermbg=161 ctermfg=white guibg=#FFD9D9
augroup overlength
  autocmd!
  autocmd FileType c,cpp,markdown,gitcommit,ruby,python,lisp
        \ match OverLength /\%>80v.\+/
  autocmd FileType javascript,typescript match OverLength /\%>80v.\+/
  autocmd FileType java match OverLength /\%>120v.\+/
augroup END

" Enable spell check
autocmd FileType markdown,gitcommit setlocal spelllang=en_ca
autocmd FileType markdown,gitcommit setlocal spell
set spellfile=$HOME/.vim-spell-en.utf-8.add
set complete+=kspell

" Ugh, tab settings
set tabstop=2 shiftwidth=2 softtabstop=2 smarttab expandtab
augroup indentsettings
  autocmd!
  autocmd FileType * setlocal tabstop=2 shiftwidth=2 softtabstop=2 smarttab expandtab
  autocmd FileType c setlocal noexpandtab cindent tabstop=8 shiftwidth=8 softtabstop=8 smarttab expandtab
  autocmd FileType python setlocal tabstop=4 shiftwidth=4 softtabstop=4
augroup END

" Stupid Windows settings
if has('win32') || has('win64')
  set wildignore+=*\\node_modules\\*,*\\tmp\\*,*.swp,*.zip,*.exe
  if !has('gui_running') && !empty($CONEMUBUILD)
    let &t_AB="\e[48;5;%dm"
    let &t_AF="\e[38;5;%dm"
    colorscheme molokai
    let g:airline_theme="molokai"
  else
    " set guifont=Hack:h10
    " set guifontwide=Hack:h10
    " if (v:version == 704 && has("patch393")) || v:version > 704
    "   set renderoptions=type:directx,level:0.75,gamma:1.25,contrast:0.25,
    "         \geom:1,renmode:5,taamode:1
    " endif
  endif
else
  set wildignore+=*/node_modules/*,*/tmp/*,*.so,*.swp,*.zip
  " set guifont=Hack\ 12
  " set guifontwide=Hack\ 12
endif

" stupid mac settings
if has('macunix')
  let g:netrw_browsex_viewer = 'open'
endif

if filereadable($HOME . '/.vim/colorscheme.vim')
  source $HOME/.vim/colorscheme.vim
else
  colorscheme base16-monokai
endif

" Sweet keymaps
augroup buffermaps
  autocmd!
  autocmd FileType html,xml :iabbrev <buffer> </ </<C-X><C-O>
augroup end

" Vimwiki
let g:vimwiki_list = [{
      \ 'path': '~/journal',
      \ 'syntax': 'markdown', 'ext': '.md',
      \ 'auto_toc': 1,
      \ 'auto_tags': 1,
      \ 'auto_diary_index': 1,
      \ 'name': 'Journal'
      \ }]
map <Leader>wc :call ToggleCalendar()<CR>

let g:vimwiki_global_ext = 0
" let g:vimwiki_listsyms = ' ○◐●✓'
" let g:vimwiki_listsym_rejected = '✗'

let g:taskwiki_markup_syntax = 'markdown'
let g:taskwiki_disable_concealcursor = 'yes'

let g:task_rc_override = 'rc.defaultwidth=0
      \ rc.defaultheight=0'

augroup vimiki
  autocmd!
  autocmd Filetype vimwiki Goyo 100
  autocmd Filetype taskreport Goyo 180
  autocmd Filetype vimwiki setl tw=80
augroup end

function! ToggleCalendar()
  execute ":Calendar"
  if exists("g:calendar_open")
    if g:calendar_open == 1
      execute "q"
      unlet g:calendar_open
    else
      g:calendar_open = 1
    end
  else
    let g:calendar_open = 1
  end
endfunction

" Airline stuff
let g:airline_powerline_fonts=1
if !exists("g:airline_symbols")
  let g:airline_symbols={}
endif
let g:airline_left_sep=""
let g:airline_left_alt_sep=""
let g:airline_right_sep=""
let g:airline_right_alt_sep=""
let g:airline_symbols.branch=""
let g:airline_symbols.readonly=""
let g:airline_symbols.linenr='¶'
let g:airline#extensions#tabline#enabled=1
let g:airline#extensions#whitespace#symbol="◎"


" Fugitive
command! Gpushf Gpush --force-with-lease

nnoremap <Leader>gm :MerginalToggle<CR>
nnoremap <Leader>gs :25Gstatus <bar> setlocal wfh<CR>
nnoremap <Leader>gw :Gwrite<CR>
nnoremap <Leader>gr :Gread<CR>
nnoremap <Leader>gp :Gpush<CR>
nnoremap <Leader>gpf :Gpushf<CR>
nnoremap <Leader>gc :Gcommit<CR>
nnoremap <Leader>gca :Gcommit --amend<CR>
nnoremap <Leader>gce :Gcommit --amend --no-edit<CR>

augroup fugitive
  autocmd!
  autocmd FileType gitcommit setlocal textwidth=72
augroup end

" Vinegar
let g:netrw_liststyle = 1
let g:netrw_fastbrowse = 0
autocmd FileType netrw setl bufhidden=wipe

" Polyglot
let g:polyglot_disabled = ['vue', 'md', 'markdown']

augroup polyglot
  autocmd!
  " autocmd FileType vue syntax sync fromstart
  autocmd BufRead *.vue set ft=html | set ft=vue
  autocmd BufEnter *.vue syntax sync fromstart
  autocmd FileType markdown syntax sync fromstart
augroup end

" FZF
if executable('rg')
  let $FZF_DEFAULT_COMMAND='rg --files'
endif

nnoremap <Leader>p :Files<CR>
nnoremap <Leader>b :Buffers<CR>
nnoremap <Leader>f :Rg<CR>

if has('float')
  " Reverse the layout to make the FZF list top-down
  let $FZF_DEFAULT_OPTS='--layout=reverse'

  " Using the custom window creation function
  let g:fzf_layout = { 'window': 'call FloatingFZF()' }

  " Function to create the custom floating window
  function! FloatingFZF()
    " creates a scratch, unlisted, new, empty, unnamed buffer
    " to be used in the floating window
    let buf = nvim_create_buf(v:false, v:true)

    " 90% of the height
    let height = float2nr(&lines * 0.5)
    " 60% of the height
    let width = float2nr(&columns * 0.6)
    " horizontal position (centralized)
    let horizontal = float2nr((&columns - width) / 2)
    " vertical position (one line down of the top)
    let vertical = float2nr(&lines * 0.25)

    let opts = {
      \ 'relative': 'editor',
      \ 'row': vertical,
      \ 'col': horizontal,
      \ 'width': width,
      \ 'height': height
      \ }
    " open the new window, floating, and enter to it
    call nvim_open_win(buf, v:true, opts)
  endfunction
endif

" Markdown
let g:markdown_enable_conceal=1
let g:vim_markdown_frontmatter=1
let g:markdown_fenced_languages = ['js=javascript', 'javascript', 'html', 'scss', 'yaml', 'css']
let g:vim_markdown_fenced_languages = ['js=javascript', 'javascript', 'html', 'scss']

" Signify
let g:signify_vsc_list = ['git']
let g:signify_realtime = 1
let g:signify_sign_add="➜"
let g:signify_sign_delete="✖"
let g:signify_sign_delete_first_line=g:signify_sign_delete
let g:signify_sign_change="●"
let g:signify_sign_changedelete=g:signify_sign_change

" vim-autocomplete
set completeopt+=preview
set completeopt-=menuone

augroup autocomplete
  autocmd!
  autocmd! CompleteDone * if pumvisible() == 0 | pclose | endif
augroup END

" ALE

set completeopt=menu,menuone,preview,noselect,noinsert

if has('nvim')
  let g:ale_virtualtext_cursor = 1
endif

let g:ale_completion_enabled = 0
let g:ale_disable_lsp = 1
let g:ale_fix_on_save = 1
let g:ale_sign_column_always = 1

let g:ale_sign_error = '✗'
let g:ale_sign_warning = '?'
let g:ale_sign_info = '✭'

let g:airline#extensions#ale#enabled = 1

let g:ale_fixers = { '*': ['remove_trailing_lines', 'trim_whitespace'] }

nmap <Leader>F <Plug>(ale_fix)
nmap <Leader>n <Plug>(ale_next_wrap)

" LSP
let g:UltiSnipsExpandTrigger = '<C-y>'
let g:UltiSnipsJumpForwardTrigger = '<tab>'
let g:UltiSnipsJumpBackwardTrigger = '<S-tab>'

source $HOME/.vim/lsp.vim

au User asyncomplete_setup call asyncomplete#register_source(asyncomplete#sources#file#get_source_options({
    \ 'name': 'file',
    \ 'whitelist': ['*'],
    \ 'priority': 10,
    \ 'completor': function('asyncomplete#sources#file#completor')
    \ }))

au User asyncomplete_setup call asyncomplete#register_source(asyncomplete#sources#emoji#get_source_options({
    \ 'name': 'emoji',
    \ 'whitelist': ['markdown', 'vimwiki', 'text'],
    \ 'completor': function('asyncomplete#sources#emoji#completor'),
    \ }))

au User aysncomplete_setup call asyncomplete#register_source(asyncomplete#sources#ultisnips#get_source_options({
    \ 'name': 'ultisnips',
    \ 'whitelist': ['vimwiki'],
    \ 'completor': function('asyncomplete#sources#ultisnips#completor'),
    \ }))


" let g:lsp_log_verbose = 1
" let g:lsp_log_file = expand('~/vim-lsp.log')


nmap <silent> gd <Plug>(lsp-definition)
nmap <silent> gy <Plug>(lsp-type-definition)
nmap <silent> gr <Plug>(lsp-references)
nmap <silent> gb <Plug>(lsp-hover)
nmap <silent> <leader>r <Plug>(lsp-rename)

imap <C-Space> <Plug>(asyncomplete_force_refresh)

" Dispatch
let g:dispatch_handlers = ['job']

" Vista
let g:vista_icon_indent = ["╰─▸ ", "├─▸ "]
let g:vista_default_executive = 'vim_lsp'
let g:vista_fzf_preview = ['right:50%']
let g:vista_finder_alternative_executives = ['ale']
nnoremap <Leader>v :Vista finder<CR>
nnoremap <Leader>V :Vista!!<CR>

let g:prosession_on_startup = 0
let g:prosession_per_branch = 1
let g:prosession_tmux_title = 1

" Nord
let g:nord_uniform_diff_background = 1
let g:nord_italic_comments = 1
let g:nord_underline = 1

" Goyo

augroup Goyo
  autocmd!
  autocmd User GoyoEnter Limelight
  autocmd User GoyoLeave Limelight!
augroup END

" Vim Test

augroup VimTest
  autocmd!
  autocmd VimEnter * if getcwd() =~ 'gdk' |
        \let test#javascript#karma#file_pattern = '\vspec/javascripts/.*\.js$' |
        \ let test#javascript#karma#executable = 'yarn karma' |
        \ let test#javascript#jest#file_pattern = '\vspec/frontend/.*\.js$' |
        \ let test#javascript#jest#executable = 'yarn jest' |
        \ endif
augroup END

nmap <silent> <leader>tn :TestNearest<CR>
nmap <silent> <leader>tf :TestFile<CR>
nmap <silent> <leader>ts :TestSuite<CR>
nmap <silent> <leader>tl :TestLast<CR>
nmap <silent> <leader>tv :TestVisit<CR>

" hexokinase

let g:Hexokinase_highlighters = ['virtual']


set mouse=
