if executable($HOME . '/.vim/lsp/elixir-ls/release/language_server.sh')
  au User lsp_setup call lsp#register_server({
        \'name': 'elixir_ls',
        \'cmd': {server_info->[$HOME . '/.vim/lsp/elixir-ls/release/language_server.sh']},
        \'root_uri':{server_info->lsp#utils#path_to_uri(lsp#utils#find_nearest_parent_file_directory(lsp#utils#get_buffer_path(),['mix.exs', '.git/']))},
        \'whitelist': ['elixir', 'eelixir']
        \})
endif

if executable('vls')
  au User lsp_setup call lsp#register_server({
        \'name': 'vue-langauge-server',
        \'cmd': {server_info-> ['vls']},
        \'root_uri':{server_info->lsp#utils#path_to_uri(lsp#utils#find_nearest_parent_file_directory(lsp#utils#get_buffer_path(), 'package.json'))},
        \'whitelist': ['vue'],
        \'config': {},
        \'initialization_options': {},
        \'workspace_config': {},
        \})
endif

if executable('typescript-language-server')
  au User lsp_setup call lsp#register_server({
        \'name': 'javascript-typescript-langauge-server',
        \'cmd': {server_info-> [&shell, &shellcmdflag, 'typescript-language-server --stdio']},
        \'root_uri':{server_info->lsp#utils#path_to_uri(lsp#utils#find_nearest_parent_file_directory(lsp#utils#get_buffer_path(), 'package.json'))},
        \'whitelist': ['javascript', 'typescript', 'jasmine.javascript'],
        \'blacklist': ['vue'],
        \})
endif

if executable('solargraph')
  au User lsp_setup call lsp#register_server({
        \'name': 'solargraph',
        \'cmd': {server_info-> [&shell, &shellcmdflag, 'solargraph stdio']},
        \'whitelist': ['ruby'],
        \})
endif

if executable('docker-langserver')
  au User lsp_setup call lsp#register_server({
        \'name': 'docker',
        \'cmd': {server_info-> [&shell, &shellcmdflag, 'docker-langserver --stdio']},
        \'whitelist': ['dockerfile'],
        \})
endif

if executable('css-languageserver')
  au User lsp_setup call lsp#register_server({
        \'name': 'css lanaguage server',
        \'cmd': {server_info-> [&shell, &shellcmdflag, 'css-languageserver --stdio']},
        \'whitelist': ['css', 'sass', 'scss', 'less'],
        \'config': {},
        \'initialization_options': {},
        \'workspace_config': {},
        \})
endif
