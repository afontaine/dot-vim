let g:ale_elixir_credo_strict = 1
let b:ale_linters = ['credo', 'dialyxir', 'mix']
let b:ale_fixers = ['mix_format']
